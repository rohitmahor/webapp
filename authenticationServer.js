const comproDLS = require('comprodls-sdk');
var bodyParser = require('body-parser');


var login = function(req,res){
    var comproDLSObj = comproDLS.init();
    console.log(req.body);
    comproDLSObj.authWithCredentials(req.body.orgName, {username:req.body.userName, password:req.body.password}, {}).then(

    function success(response) {
      //You may persist token object in session/localstorage etc. for later usage.    	 
      var token = response["token"];
      console.log(token.expires_in);
       
      //user object contains user information
      var user = response["user"];
      console.log(user.name);
      console.log(user.roles);
      
      //org object contains organisation information
      var org = response["user"]["org"];
      console.log(org["type"]);
      res.send({"success":true,response});

    },
    function error(err) {
      var type = err["type"];
      if (type == "API_ERROR") {
        if (err["httpcode"] == 401) {
          //Invalid Credentials
          console.log(err.message);
        }
      } else  if (type == "SDK_ERROR") {
        console.log(err.message);
      }
      res.send({"success":false,response});

    }
);
}

exports.login = login;