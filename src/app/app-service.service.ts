import { Injectable } from '@angular/core';
import { users } from './Samples/dummyUsers'
import { Observable } from 'rxjs';
import { of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

@Injectable()
export class AppService {

  constructor() { }
  static currentUser;
  static currentUserBooks;
  static allUsers = users;

  getUsers(): Observable<any[]> {
    console.log(users);
    return of(users);
  }

  setCurrentUser(userName) {
    AppService.currentUser = userName;
  }

  static getCurrentUser() {
    return AppService.currentUser;
  }

  static getCurrentUserBooks() {
    console.log(AppService.allUsers);
    let userDataNode = AppService.allUsers.find(user => AppService.currentUser == user.username);
    return userDataNode && userDataNode.data.books;
  }
}

