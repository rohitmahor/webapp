import { Component, OnInit } from '@angular/core';
import { LoginService } from '../login.service'
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { users } from '../Samples/dummyUsers';
import { ApplicationRef } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';



@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {


  currentUser = "";
  loginStatus = false;
  allUsers = users;
  books;

  constructor(private route: ActivatedRoute,
    private router: Router,
    private loginService: LoginService,
    private ref: ApplicationRef,
    private cookieService: CookieService) { }


  getCurrentUser(): void {
    var cookieValue = this.cookieService.get('data')
    this.currentUser = cookieValue.split('-')[0];

  }


  logOut() {

    this.cookieService.delete('data', '/', window.location.hostname)

    this.router.navigate(['/login']);

  }
  ngOnInit() {


    this.loginService.isAuthenticated().subscribe((data) => {
      if (data['isAuthenticated']) {
        this.loginStatus = true;
        this.ref.tick();

        this.getCurrentUser();


        this.books = this.allUsers.find(user => {
          if (user.username.toLowerCase() == this.currentUser.toLowerCase()) {
            return true;
          }
        }).data.books;
      }
      else {
        this.router.navigate(['/login']);
      }
    },
      err => {
        if (err.status == 401) {
          this.router.navigate(['/login']);
        }
      }
    )
  }

}
