import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class LoginService {

  constructor(private http: HttpClient) { }

  login(username: string, password: string): Observable<any> {
    var user = { "username": username, "password": password }
    const headers = new HttpHeaders()
      .set('Content-Type', 'application/json');
    return this.http.post(`/api/users/login`, user, {
      headers: headers
    })
  }

  logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('currentUser');
  }

  isAuthenticated() {
    return this.http.get(`/api/users/isAuthenticated`)
  }
}

